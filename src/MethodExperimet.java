import java.util.Scanner;

public class MethodExperimet {
    public static void main(String[] args) {
        String[] menu = {"Tempra", "Ramen", "Udon"};
        int i;
        Scanner sc = new Scanner(System.in);
        int order;

        System.out.println("What would you like to order:");
        for (i = 0; i < menu.length; i++) {
            System.out.println(i+1 + ". " + menu[i]);
        }

        order = ScanOrenernum(sc, menu.length);

        order(menu[order-1]);
    }

    static int ScanOrenernum(Scanner sc, int max){
        int order;
        while (true) {
            System.out.print("Your order [1-"+max+"]:");
            try {
                order = sc.nextInt();
                if (order < 1 || order > max)
                    throw new Exception();
            } catch (Exception e) {
                sc.nextLine();
                System.out.println("Please enter a number between 1 and " + max + ".");
                continue;
            }
            break;
        }
        return order;
    }

    static void order(String food){
        String prefix = "You have ordered ";
        String suffix = ". Thank you!";
        System.out.println(prefix + food + suffix);
    }
}
